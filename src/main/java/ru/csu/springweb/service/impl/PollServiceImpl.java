package ru.csu.springweb.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.csu.springweb.dto.Poll;
import ru.csu.springweb.mapper.PollMapper;
import ru.csu.springweb.repository.PollRepository;
import ru.csu.springweb.service.PollService;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PollServiceImpl implements PollService {

    private final PollRepository pollRepository;
    private final PollMapper pollMapper;

    @Override
    public void createPoll(Poll poll) {
        pollRepository.save(pollMapper.mapSource(poll));
    }

    @Override
    public void deletePoll(int id) {

    }

    @Override
    public ru.csu.springweb.model.Poll getPoll(long id) {
        return pollRepository.findById(id)
                .map(p -> ru.csu.springweb.model.Poll
                        .builder()
                        .id(p.getId())
                        .pollName(p.getPollName())
                        .active(p.getActive())
                        .startDate(p.getStartDate())
                        .endDate(p.getEndDate())
                        .build())
                .orElseThrow(() -> {
                    throw new EntityNotFoundException("Запрашеваемый ресурс не найден");
                });
    }

    @Override
    public void addQuestionToPoll(long pollId) {

    }

    @Override
    public void deleteQuestion(long id) {

    }

    @Override
    public List<ru.csu.springweb.model.Poll> findPollsByDate(LocalDate start, LocalDate end) {
        return null;
    }
}

package ru.csu.springweb.mapper;

import java.util.List;

public interface AbstractMapper<S, T> {
    T mapSource(S source);

    S mapTarget(T target);

    List<S> mapTarget(List<T> target);

    List<T> mapSource(List<S> source);
}
